## Installation

- `composer install`
- create `.env` file based on `.env.example`
- put your `NYT_API_KEY` in `.env` file

## Testing  

- `php artisan test`

## Ideas and Observations

- I assumed that what I get from NYT Books API should be returned by our API endpoint - but in real application output from NYT API should be processed and checked before returning it to the users

- I skipped on checking errors coming from NYT API to simplify 
    - also I had trouble getting an error from that API
    - other than not providing api-key

- I think there is something wrong with ISBN parameter of NYT Books API `/lists/best-sellers/history.json` endpoint 
    - if you provide one ISBN it works correctly and returns 1 book data
    - if you provide two or more ISBN codes (separated by semicolons) of different books - zero results
    - if you provide two or more ISBN codes (separated by semicolons) of the SAME books - zero results
    - so I am not sure what I am doing wrong here or what is the purpose of this parameter
