<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Rules\ISBNValidationRule;

class NYTController extends Controller
{
    const API_BASE_URL = 'https://api.nytimes.com/svc/books/v3';
    const API_BEST_SELLERS_URL = self::API_BASE_URL . '/lists/best-sellers/history.json';

    /**
     * Display a listing of the resource.
     */
    public function bestSellers(Request $request)
    {
        $attributes = $request->validate([
            'author' => 'sometimes|min:1|string',
            'title' => 'sometimes|min:1|string',
            'offset' => 'sometimes|multiple_of:20',
            'isbn' => 'sometimes|array',
            'isbn.*' => ['sometimes', new ISBNValidationRule()],
        ]);
        
        if (isset($attributes['isbn'])) {
            $attributes['isbn'] = implode(';', array_map(fn($value) => strtr($value, '-', ''), $attributes['isbn']));
        }
        $attributes['api-key'] = env('NYT_API_KEY');

        return Http::get(self::API_BEST_SELLERS_URL, $attributes);
    }
}
