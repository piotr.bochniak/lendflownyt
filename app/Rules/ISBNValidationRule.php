<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ISBNValidationRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        // found at - https://stackoverflow.com/questions/41271613/use-regex-to-verify-an-isbn-number
        // should use something like - https://github.com/nicebooks-com/isbn - for better validation
        if (!preg_match('/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/', $value)) {
            $fail('The :attribute must be a valid ISBN number.');
        }
    }
}
