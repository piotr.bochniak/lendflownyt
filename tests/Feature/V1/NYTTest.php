<?php

namespace Tests\Feature\V1;

use Illuminate\Support\Facades\Http;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;

class NYTTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Http::fake([
            'api.nytimes.com/*' => Http::response(['status' => 'OK'], 200),
        ]);
    }

    public function test_v1_nyt_no_parameters(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers'));

        $response->assertStatus(200);
    }

    public function test_v1_nyt_correct_parameters(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'title' => 'Test',
            'author' => 'Test',
            'isbn' => ['9780785196570', '1451627289'],
            'offset' => 40,
        ]));

        $response->assertStatus(200);
    }

    public function test_v1_nyt_with_wrong_offset(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'offset' => 39
        ]));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'offset',
            ]
        ]);
    }

    public function test_v1_nyt_with_wrong_isbn_type(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'isbn' => '9780785196570'
        ]));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'isbn',
            ]
        ]);
    }

    public function test_v1_nyt_with_wrong_isbn_value(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'isbn' => ['100', '9780785196570', '300']
        ]));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'isbn.0',
                'isbn.2'
            ]
        ]);
    }

    public function test_v1_nyt_with_wrong_author(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'author' => '',
        ]));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'author'
            ]
        ]);
    }

    public function test_v1_nyt_with_wrong_title(): void
    {
        $response = $this->getJson(route('api.v1.nyt.best-sellers', [
            'title' => '',
        ]));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'title'
            ]
        ]);
    }
}
